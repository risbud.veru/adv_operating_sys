#include<bits/stdc++.h>
#include<pthread.h>
#include<unistd.h>
#include<semaphore.h>

using namespace std;

int state[5];
vector<int> ph = {1, 2, 3, 4, 5};
vector<int> q;

sem_t mut;
sem_t S[5];

void test(int ph) {
    cout<<endl;
    if(state[ph] == 1 && state[(ph+4)%5] != 0 && state[(ph+1)%5] != 0) {
	state[ph] = 0;
	sleep(2);
	cout<<"Philosopher "<<ph<<" is taking "<<(ph+4)%5<<" and "<<ph<<" fork."<<endl;
	cout<<"Philosopher "<<ph<<" is eating"<<endl;
	sem_post(&S[ph]);
    }
}

void pickup(int ph) {
    cout<<endl;
    sem_wait(&mut);
    state[ph] = 1;
    cout<<"Philosopher "<<ph<<" is hungry"<<endl;
    sem_post(&mut);
    sem_wait(&S[ph]);

    if(q.empty())
    	test(ph);
    else {
	q.push_back(ph);
	while(q.empty());
    	test(ph);
    }	
    
    sleep(1);
}

void putdown(int ph) {
    cout<<endl;
    sem_wait(&mut);
    state[ph] = 2;
    if(find(q.begin(), q.end(), ph)!=q.end())
    	q.erase(remove(q.begin(), q.end(), ph), q.end());
    cout<<"Philosopher "<<ph<<" is putting fork "<<(ph+4)%5<<" and "<<ph<<" on table."<<endl;
    test((ph+4)%5);
    test((ph+1)%5);
    sem_post(&mut);
}

void* philo(void* n) {
    while(1) {
        cout<<endl;
	int* i = (int *)n;
	sleep(1);
	pickup(*i);
	sleep(0);
	putdown(*i);	
    }
}

int32_t main() {
    pthread_t tid[5];
    sem_init(&mut, 0, 1);
    for (int i = 0; i < 5; i++)
        sem_init(&S[i], 0, 0);
    for(int i = 0; i < 5; i++) {
    	pthread_create(&tid[i], NULL, philo, &ph[i]);
	cout<<"Philosopher "<<i<<" is thinking "<<endl;;
    } 
    for(int i = 0; i < 5; i++)
	pthread_join(tid[i], NULL);
    return  0;
}
