#include<bits/stdc++.h>
#include<thread>
#include<chrono>

using namespace std;
int var = 1;
class semaphore {
    public:
    int ctrl = 10;
    void wait() {
    	while(!ctrl);
	ctrl = 0;
    }
    void signal() {
    	while(ctrl);
	ctrl = 1;
    }
};


void m1(semaphore s) {
    while(true) {
    	s.wait();
    	std::this_thread::sleep_for(std::chrono::milliseconds(1000));
    	cout<<"Updated in m1 : "<<++var<<endl;
    	s.signal();
    }
}

void m2(semaphore s) {
    while(true) {
	s.wait();
    	std::this_thread::sleep_for(std::chrono::milliseconds(3000));
	cout<<"updated in m2 : "<<++var<<endl;
	s.signal();
    }
}

void m3(semaphore s) {
    while(true) {
	s.wait();
    	std::this_thread::sleep_for(std::chrono::milliseconds(5000));
	cout<<"updated in m3 : "<<++var<<endl;
	s.signal();
    }
}
int main() {
    semaphore s;
    std::thread t1(m1, s);
    std::thread t2(m2, s);
    std::thread t3(m3, s);
    t1.join();
    t2.join();
    t3.join();
    return 0;
}
