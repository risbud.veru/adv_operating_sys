#include <stdio.h>
#include <unistd.h> 
#include <semaphore.h>
#include <pthread.h>

int state[5];
int phil[5] = {0, 1, 2, 3, 4};
 
sem_t mutex;
sem_t S[5];
 
void test(int ph)
{
    if (state[ph] == 1 && state[(ph+4)%5] != 0 && state[(ph+1)%5] != 0) {
        state[ph] = 0;
        sleep(2);
        printf("\nphilosopher %d takes fork %d and %d\n", ph+1, (ph+4)%5+1, ph+1);
        printf("\nphilosopher %d Eating\n", ph+1);
        sem_post(&S[ph]);
    }
}
 
void pick_up(int ph)
{
    sem_wait(&mutex);
    state[ph] = 1;
    printf("\nPhilosopher %d Hungry\n", ph+1);
    test(ph);
    sem_post(&mutex);
    sem_wait(&S[ph]);
    sleep(1);
}
 
void put_down(int ph)
{
    sem_wait(&mutex);
    state[ph] = 2;
    printf("Philosopher %d putting fork %d and %d down\n",ph+1, (ph+4)%5+1, ph+1);
    printf("Philosopher %d thinking\n", ph + 1);
    test((ph+4)%5);
    test((ph+1)%5);
    sem_post(&mutex);
}
 
void* philospher(void* num)
{
    while (1) {
        int* i = num;
        sleep(1);
        pick_up(*i);
        sleep(0);
        put_down(*i);
    }
}
 
int32_t main()
{
    int i;
    pthread_t thread_id[5];
    sem_init(&mutex, 0, 1);
    for (i = 0; i < 5; i++)
        sem_init(&S[i], 0, 0);
    for (i = 0; i < 5; i++) {
        pthread_create(&thread_id[i], NULL, philospher, &phil[i]);
        printf("\nPhilosopher %d thinking", i+1);
    }
    for (i = 0; i < 5; i++)
        pthread_join(thread_id[i], NULL);

    return 0;
}




//Galvin pg 237
